package pl.com.tok.mcc;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * wyświetlanie danych wykonanych ruchów i wyniku gry
 * @author tomek
 */

public class Statistics {

    protected static void getMovesStatistics(TreeMap<String,Element> allElements, int loop, int mice) {
          
        System.out.println("Tura: "+loop+" - pozostało: "+mice+" myszy.");

        for (Map.Entry<String,Element> check : allElements.entrySet()) {   
            if(!check.getValue().getName().equals("X")){
            System.out.println(check.getValue().getName() + check.getValue().getnumb()
                    + "(" + check.getValue().getPrevX() + "," + check.getValue().getPrevY()
                    + ")->(" + check.getValue().getPosX() + "," + check.getValue().getPosY() + ")");
            }
        }
    }  
}
