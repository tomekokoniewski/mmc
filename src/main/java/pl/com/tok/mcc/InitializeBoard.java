package pl.com.tok.mcc;

/**
 * inicjalizacja początkowego stanu gry
 * @author tomek 
 */

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import static pl.com.tok.mcc.Rules.setPossibleDir;

public class InitializeBoard {

    static final int CENTER_OF_BOARD = Main.BOARD_SIZE / 2;    
    
    protected static void initialize(TreeMap<String,Element>  allElements, String[][] board) {
        Random r = new Random();
        int x = r.nextInt(Main.BOARD_SIZE);
        int y = r.nextInt(Main.BOARD_SIZE);
        
        //--WYPEŁNIENIE PLANSZY KROPKAMI 
        for (int xx = 0; xx < Main.BOARD_SIZE; xx++) {
            for (int yy = 0; yy < Main.BOARD_SIZE; yy++) {
                board[xx][yy] = ".";
            }
        }

        //-- dodanie sera
        Cheese c = new Cheese(CENTER_OF_BOARD, CENTER_OF_BOARD);
        allElements.put(c.name+c.getnumb(), c);
        board[CENTER_OF_BOARD][CENTER_OF_BOARD] = "X";

        //--DODANIE KOTÓW
        int i = 0;
        do {
            Animal a = new Animal(Elements.C, i, x, y, 0,0);
            boolean exist = false;

                         
            for (Map.Entry<String,Element> check : allElements.entrySet()) {
                if (!exist) {
                    exist = ((a.getPosX() == check.getValue().getPosX() && a.getPosY() == check.getValue().getPosY()) || (a.getPosX() == c.getPosX() && a.getPosY() == c.getPosY())) ? true : false; //Kot nie jest na pozycji innego kota i nie jest na pozycji sera
                }
            }

            if (!exist) {
                setPossibleDir(a);
                allElements.put(a.name+a.getnumb(), a);
                board[y][x] = "C";
                i++;
            }
            x = r.nextInt(Main.BOARD_SIZE);
            y = r.nextInt(Main.BOARD_SIZE);

        } while (i < Main.MAX_CATS);

        //--DODANIE MYSZ
        i = 0;
        do {
            Animal a = new Animal(Elements.M, i, x, y, 0,0);
            boolean exist = false;

            for (Map.Entry<String,Element> check : allElements.entrySet()) {
                if (!exist) {
                    exist = ((a.getPosX() == check.getValue().getPosX() && a.getPosY() == check.getValue().getPosY()) || (a.getPosX() == c.getPosX() && a.getPosY() == c.getPosY())) ? true : false; //Kot nie jest na pozycji innego kota i nie jest na pozycji sera
                }
            }

            if (!exist) {
                setPossibleDir(a); 
                allElements.put(a.name+a.getnumb(), a);
                board[y][x] = "M";
                i++;
            }
            x = r.nextInt(Main.BOARD_SIZE);
            y = r.nextInt(Main.BOARD_SIZE);

        } while (i < Main.MAX_MICE);
    }
}
