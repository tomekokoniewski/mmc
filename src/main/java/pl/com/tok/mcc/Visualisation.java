package pl.com.tok.mcc;

import java.util.Map;
import static pl.com.tok.mcc.Game.allElements;
import static pl.com.tok.mcc.Game.board;

/**
 * wyrysowanie na ekranie wizualizacji stanu gry
 * @author tomek
 */

public class Visualisation {

    // wyrysowanie stanu gry
    protected static void visualise(String[][] board) {
      
        for (int x = 0; x < Main.BOARD_SIZE; x++) {
            for (int y = 0; y < Main.BOARD_SIZE; y++) {
                System.out.print(board[x][y]+"\t");
            }
            System.out.print("\n");
        }

    }

    // zaktualizowanie stanu gry w talicy na podst której dokonywana jest wizualizacja
    protected static void updateBoard() {
         
        for (int x = 0; x < Main.BOARD_SIZE; x++) {
            for (int y = 0; y < Main.BOARD_SIZE; y++) {
                board[x][y] = ".";
            }
        }

          for (Map.Entry<String, Element> currElement : allElements.entrySet()) {        
            board[currElement.getValue().getPosY()][currElement.getValue().getPosX()] = currElement.getValue().name;
        }

    }
}
