package pl.com.tok.mcc;

import java.util.TreeMap;

/**
 * zasady ruchu zwierząt
 * @author tomek
 */

public class Rules {
    //ogólne zasady dla Myszy i Kotów
    public static void setPossibleDir(Element a) {  
        int x = a.getPosX();
        int y = a.getPosY();
        TreeMap<Integer, String> possibleDir = new TreeMap();

        if (a.getName().equals("M")) {
            if (x > 0 && y > 0 && x < Main.BOARD_SIZE - 1 && y < Main.BOARD_SIZE - 1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "NE");
                possibleDir.put(2, "E");
                possibleDir.put(3, "SE");
                possibleDir.put(4, "S");
                possibleDir.put(5, "SW");
                possibleDir.put(6, "W");
                possibleDir.put(7, "NW");
                a.setPossibleDir(possibleDir);
            } else if (x == 0 && y == 0) {
                possibleDir.put(0, "E");
                possibleDir.put(1, "S");
                possibleDir.put(2, "SE");
                a.setPossibleDir(possibleDir);
            } else if (x == Main.BOARD_SIZE-1 && y == Main.BOARD_SIZE-1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "NW");
                possibleDir.put(2, "W");
                a.setPossibleDir(possibleDir);
            } else if (x == 0 && y == Main.BOARD_SIZE-1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "NE");
                possibleDir.put(2, "E");
                a.setPossibleDir(possibleDir);
            } else if (x == Main.BOARD_SIZE-1 && y == 0) {
                possibleDir.put(0, "S");
                possibleDir.put(1, "SW");
                possibleDir.put(2, "W");
                a.setPossibleDir(possibleDir);
            } else if (y == 0 && x > 0 && x < Main.BOARD_SIZE - 1) {
                possibleDir.put(0, "E");
                possibleDir.put(1, "ES");
                possibleDir.put(2, "S");
                possibleDir.put(3, "SW");
                possibleDir.put(4, "W");
                a.setPossibleDir(possibleDir);
            } else if (x == 0 && y > 0 && y < Main.BOARD_SIZE - 1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "NE");
                possibleDir.put(2, "E");
                possibleDir.put(3, "SE");
                possibleDir.put(4, "S");
                a.setPossibleDir(possibleDir);
            } else if (y == Main.BOARD_SIZE - 1 && x > 0 && x < Main.BOARD_SIZE - 1) {
                possibleDir.put(0, "W");
                possibleDir.put(1, "NW");
                possibleDir.put(2, "N");
                possibleDir.put(3, "NE");
                possibleDir.put(4, "E");
                a.setPossibleDir(possibleDir);
            } else if (x == Main.BOARD_SIZE - 1 && y > 0 && y < Main.BOARD_SIZE - 1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "NW");
                possibleDir.put(2, "W");
                possibleDir.put(3, "SW");
                possibleDir.put(4, "S");
                a.setPossibleDir(possibleDir);
            }
        } else if (a.getName().equals("C")) {
            if (x > 0 && y > 0 && x < Main.BOARD_SIZE - 1 && y < Main.BOARD_SIZE-1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "E");
                possibleDir.put(2, "S");
                possibleDir.put(3, "W");
                a.setPossibleDir(possibleDir);
            } else if (x == 0 && y == 0) {
                possibleDir.put(0, "E");
                possibleDir.put(1, "S");
                a.setPossibleDir(possibleDir);
            } else if (x == Main.BOARD_SIZE-1 && y == Main.BOARD_SIZE-1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "W");
                a.setPossibleDir(possibleDir);
            } else if (x == 0 && y == Main.BOARD_SIZE-1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "E");
                a.setPossibleDir(possibleDir);
            } else if (x == Main.BOARD_SIZE-1 && y == 0) {
                possibleDir.put(0, "S");
                possibleDir.put(1, "W");
                a.setPossibleDir(possibleDir);
            } else if (y == 0 && x > 0 && x < Main.BOARD_SIZE-1) {
                possibleDir.put(0, "E");
                possibleDir.put(1, "S");
                possibleDir.put(2, "W");
                a.setPossibleDir(possibleDir);
            } else if (x == 0 && y > 0 && y < Main.BOARD_SIZE-1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "E");
                possibleDir.put(2, "S");
                a.setPossibleDir(possibleDir);
            } else if (y == Main.BOARD_SIZE-1 && x > 0 && x < Main.BOARD_SIZE-1) {
                possibleDir.put(0, "W");
                possibleDir.put(1, "N");
                possibleDir.put(2, "E");
                a.setPossibleDir(possibleDir);
            } else if (x == Main.BOARD_SIZE-1 && y > 0 && y < Main.BOARD_SIZE-1) {
                possibleDir.put(0, "N");
                possibleDir.put(1, "W");
                possibleDir.put(2, "S");
                a.setPossibleDir(possibleDir);
            }
        }

    }
    //Zasady modyfikacji współrzędnej X
    public static int getNewX(int x, String direction) {  
        
        if(direction.equals("NE")||direction.equals("E")||direction.equals("SE")){
           x = x+1; 
        }else if(direction.equals("NW")||direction.equals("W")||direction.equals("SW")){
           x=x-1; 
        }
        return x;
    }
    //Zasady modyfikacji współrzędnej Y
    public static int getNewY(int y, String direction) {  
        
        if(direction.equals("SE")||direction.equals("S")||direction.equals("SW")){
           y = y+1; 
        }else if(direction.equals("NW")||direction.equals("N")||direction.equals("NE")){
           y=y-1; 
        }
        return y;
    }

}
