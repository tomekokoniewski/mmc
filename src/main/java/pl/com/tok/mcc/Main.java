package pl.com.tok.mcc;

/**
 * główne parametry wejściowe gry + uruchomienie
 * @author tomek
 */
public class Main {

    protected static final int BOARD_SIZE = 5;
    protected static final int MAX_MICE = 4;
    protected static final int MAX_CATS = 4;
    protected static final int DRAW_BOARD_PERIOD = 2;
    public static boolean isFinished = false;

    public static void main(String[] args) {

        while (!isFinished) {
            Game.play();
        }
    }

}
