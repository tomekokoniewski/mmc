package pl.com.tok.mcc;

/**
 * obsługa gry
 * @author tomek
 */

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;
import static pl.com.tok.mcc.InitializeBoard.initialize;
import static pl.com.tok.mcc.Visualisation.visualise;
import static pl.com.tok.mcc.Visualisation.updateBoard;
import static pl.com.tok.mcc.Statistics.getMovesStatistics;

public class Game {

    static String[][] board = new String[Main.BOARD_SIZE][Main.BOARD_SIZE]; //ulubiony obiekt Harrego Pottera - tablica charów ;) - do wizualizacji stanu gry
    static TreeMap<String, Element> allElements = new TreeMap<>(); //lista aktualnych elementów gry z ich właściwościami
    static ArrayList<CaughtMice> caughtMiceArray = new ArrayList<>();
    static int noOfMice = 0;
    static boolean miceWon, catOnCheese;

    public static void play() {
        int loop = 1;
        //-- zainicjowanie planszy kotami i myszami, zwizualizowanie położenia i statystyk
        initialize(allElements, board);
        getMovesStatistics(allElements, loop, Main.MAX_MICE);
        visualise(board);

        while (!Main.isFinished) {
            loop++;

            //-- wykonanie ruchów, 
            for (Map.Entry<String, Element> currElement : allElements.entrySet()) {
                if (!currElement.getValue().name.equals("X")) {
                    Rules.setPossibleDir(currElement.getValue());
                    currElement.getValue().makeMove();
                }
            }

            //-- sprawdzenie stanu gry
            result();

            //-- podanie info o: turze, pozostałych myszach, ruchach
            getMovesStatistics(allElements, loop, (noOfMice - caughtMiceArray.size()));

            //-- usunięcie złapanych myszy i podanie wyniku złapanych myszy
            if (caughtMiceArray.size() > 0) {
                for (CaughtMice pair : caughtMiceArray) {
                    System.out.println("Kot nr: " + pair.getCat().getnumb() + " złapał mysz nr: " + pair.getMouse().getnumb());
                    allElements.remove(pair.getId());
                }
            }

            //--wyświetlenie wizualizacji 
            updateBoard(); //zaktualizowanie stanu gry w talicy na podst której dokonywana jest wizualizacja
            if (loop % Main.DRAW_BOARD_PERIOD == 0) {
                visualise(board);
            }

            //--jeżeli ktoś wygrał - wyświetlenie info i zatrzymanie gry
            if (noOfMice == 0) {
                System.out.println("KOTY wygrały !");
                Main.isFinished = true;
            } else if (miceWon && noOfMice == 1 && catOnCheese) {
                System.out.println("REMIS !");
                Main.isFinished = true;
            } else if (miceWon) {
                System.out.println("MYSZY wygrały !");
                Main.isFinished = true;
            }
        }
    }

    public static void result() {
        noOfMice = 0;
        caughtMiceArray.clear();
        for (Map.Entry<String, Element> check : allElements.entrySet()) {
            if (!check.getValue().getName().equals("X")) {
                if (check.getValue().getName().equals("C")) {
                    for (Map.Entry<String, Element> mouse : allElements.entrySet()) {
                        if (mouse.getValue().getName().equals("M") && mouse.getValue().getPosX() == check.getValue().getPosX()
                                && mouse.getValue().getPosY() == check.getValue().getPosY()) {
                            CaughtMice caughtMice = new CaughtMice((mouse.getValue().getName()+mouse.getValue().getnumb()), check.getValue(), mouse.getValue());
                            
                            caughtMiceArray.add(caughtMice);
                        }
                    }
                }

                //sprawdzenie czy kot na serze
                if ((check.getValue().getName().equals("C")) && (check.getValue().getPosX() == InitializeBoard.CENTER_OF_BOARD)
                        && (check.getValue().getPosY() == InitializeBoard.CENTER_OF_BOARD)) {
                    catOnCheese = true;
                }
                //sprawdzenie ile pozostało myszy
                if (check.getValue().name.equals("M")) {
                    noOfMice++;
                }
                //sprawdzenie czy mysz dotarła do sera 
                if ((check.getValue().getName().equals("M")) && (check.getValue().getPosX() == InitializeBoard.CENTER_OF_BOARD)
                        && (check.getValue().getPosY() == InitializeBoard.CENTER_OF_BOARD)) {
                    miceWon = true;
                }
            }
        }
    }
}
