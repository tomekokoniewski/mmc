package pl.com.tok.mcc;

import java.util.TreeMap;

/**
 * Klasa główna - po niej dziedziczą klasy: Animal i Chees
 * @author tomek
 */

public abstract class Element {
    
    protected String name;
    protected int numb;
    protected int posX;
    protected int posY;
    protected int prevX;
    protected int prevY;
    protected TreeMap<Integer, String> possibleDir;

    

    public  abstract void makeMove();
    
    
    public TreeMap<Integer, String> getPossibleDir() {return possibleDir;}
    public void setPossibleDir(TreeMap<Integer, String> possibleDir) {this.possibleDir = possibleDir;}

    public int getPrevX() {return prevX;}
    public void setPrevX(int prevX) {this.prevX = prevX;}

    public int getPrevY() {return prevY;}
    public void setPrevY(int prevY) {this.prevY = prevY;}
    
    public int getPosX() {return posX;}
    public void setPosX(int posX) {this.posX = posX;}

    public int getPosY() {return posY;}
    public void setPosY(int posY) {this.posY = posY;}

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    
    public int getnumb() {return numb;}
    public void setNumb(int numb) {this.numb = numb;}
    
 

}
