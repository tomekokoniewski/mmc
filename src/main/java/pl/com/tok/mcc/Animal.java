package pl.com.tok.mcc;

import java.util.Random;

/**
  * Klasa zwierzęcia - kota lub myszy
  * @author tomek
 */
public class Animal extends Element {

    public Animal(Elements e, int numb, int posX, int posY, int prevX, int prevY){
        this.name = e.toString();
        this.numb = numb;
        this.posX = posX;
        this.posY = posY;
        this.prevX= prevX;
        this.prevY= prevY;
        
    }
    
    
    @Override
    public void makeMove(){
        this.prevX = posX;
        this.prevY = posY;
        
        int nowyX= posX;
        int nowyY=posY;
        
        Random r = new Random();
        int choice = r.nextInt(this.possibleDir.size());
        String direction = this.possibleDir.get(choice);
        
        nowyX = Rules.getNewX(this.posX,direction);
        this.posX=nowyX;
        nowyY = Rules.getNewY(this.posY,direction);
        this.posY=nowyY;
    }


}
