package pl.com.tok.mcc;

/**
 * Klasa utworzona aby tworzyć obiekty par "KOT-(złapana)MYSZ" w celu ich wylistowania 
 * @author tomek
 */
public class CaughtMice{
    
    private String id; //id myszy (np. M1) w celu usunięcia jej z TreeMap allElements
    private Element cat;
    private Element mouse;
    
    public CaughtMice(String id, Element cat, Element mouse){
     this.id=id;
     this.cat=cat;
     this.mouse=mouse;
    }

   
    public String getId(){return id;}
    public Element getCat(){return cat;}
    public Element getMouse(){return mouse;}
    public void setId(String id){this.id=id;}
    public void setCat(Element cat){this.cat=cat;}
    public void setMouse(Element mouse){this.mouse=mouse;}
}
